const rootQuery = require('./graphQL-query').rootQuery;
const GraphQLSchema = require('graphql').GraphQLSchema;
const GraphQLObjectType = require('graphql').GraphQLObjectType;
const mutation = require('./graphQL-mutation.js');


module.exports = new GraphQLSchema({
	query : rootQuery,
	mutation: new GraphQLObjectType({
		name: 'Mutation',
		fields: mutation
	})
});
