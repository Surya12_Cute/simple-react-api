const {
	GraphQLString,
	GraphQLInt,
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLNonNull,
	GraphQLList
} = require('graphql');

exports.queryType = new GraphQLObjectType ({
	name: 'Makan_OreoType',
	fields: () => {
		return {
			id: {
				type : GraphQLString
			},
			name : {
				type : GraphQLString
			},
			description : {
				type : GraphQLString
			}
		}
	}
});

