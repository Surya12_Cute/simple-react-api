const {
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema
} = require("graphql");

const queryType = require("./../type.js").queryType;
const mysqlConnection = require("./../../../graphQL-query.js").mysqlConnection;

var database_name = require("./../query.js").database_name;

exports.Delete = {
	type : queryType,
	args : {
		WHEREid : {
			type : GraphQLNonNull(GraphQLString)
		}
	},
	resolve(parentValue, args){
		var Query = "Delete FROM " + database_name +" WHERE id='" +  args.WHEREid + "';";
		if(args.WHEREid){
			return getData({ query: Query }).then(value => value);
		}
	}
}
