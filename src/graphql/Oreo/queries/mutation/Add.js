const {
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema
} = require("graphql");

const queryType =require("./../type.js").queryType;

const mysqlConnection  = require('./../../../graphQL-query.js').mysqlConnection;

var database_name = require("./../query.js").database_name;


exports.Add = {
	type : queryType,
	args : {
		name : {
			type : GraphQLString
		},
		description : {
			type : GraphQLString
		}
	},
	resolve(parentValue, args){
		let RowsData = [
			{ name: "name",           value: args.name           },
			{ name: "description",    value: args.description    },
		];

		//combine all agrument into query 
	var Query =
		"INSERT INTO "
		+ database_name
		+ "(";
		var argumentChecker = false;
		for(let x = 0; x < RowsData.length; x++){
			if(RowsData[x].value){
				Query += RowsData[x].name + ",";
				argumentChecker = true;
			}
		}
		if(argumentChecker){
			// this will remove "," at the end of rows or else it will create error
			Query = Query.substring(0, Query.length - 1) + Query.substring(Query.length);
		}
		Query +=  ") VALUES (";
		var StringChecker = false;
		for(let x = 0; x < RowsData.length; x++){
			if(RowsData[x].value){
				Query += "'" + RowsData[x].value + "',";
				argumentChecker = true;
			}
		}
		if(argumentChecker){
			// this will remove "," at the end of rows or else it will create error
			Query = Query.substring(0, Query.length - 1) + Query.substring(Query.length);
		}
		Query += ");" ;




		return getData({ query: Query }).then(value => value);
	}
}
